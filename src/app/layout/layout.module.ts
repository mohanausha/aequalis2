import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './uicomponents/header/header.component';
import { FooterComponent } from './uicomponents/footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    NgbModule
  ],
  declarations: [
    LayoutComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ]
})
export class LayoutModule { }
